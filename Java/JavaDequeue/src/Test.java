import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque deque = new ArrayDeque<>();
        int n = in.nextInt();
        int m = in.nextInt();
        int uniqueQuantity = 0;

        for (int i = 0; i < n; i++) {
            int num = in.nextInt();
            if (deque.size() < m) {
                deque.addLast(num);
            } else {
                uniqueQuantity = (int) deque.stream().distinct().count();
                if (uniqueQuantity == m) {
                    break;
                }
                deque.removeFirst();
                deque.addLast(num);

            }
        }

        System.out.println(uniqueQuantity);
    }
}
