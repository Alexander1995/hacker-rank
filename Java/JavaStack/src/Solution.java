import java.util.Scanner;
import java.util.Stack;

/*
A string containing only parentheses is balanced if the following is true: 1. if it is an empty string 2.
if A and B are correct, AB is correct, 3. if A is correct, (A) and {A} and [A] are also correct.

Examples of some correctly balanced strings are: "{}()", "[{()}]", "({()})"
Examples of some unbalanced strings are: "{}(", "({)}", "[[", "}{" etc.
Given a string, determine if it is balanced or not.

There will be multiple lines in the input file, each having a single non-empty string. You should read input till end-of-file.
The part of the code that handles input operation is already provided in the editor.

 */
public class Solution {
    public static void main(String []argh) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            String input=sc.next();
            char[] symbols = input.toCharArray();
            Stack<Character> stack = new Stack<>();
            boolean result = true;
            for (char symbol : symbols) {
                if (symbol == '{' || symbol == '(' || symbol == '[') {
                    stack.push(symbol);
                } else if (stack.size() != 0) {
                    if (symbol == '}') {
                        if (stack.pop() != '{') {
                            result = false;
                            break;
                        }
                    } else if (symbol == ']') {
                        if (stack.pop() != '[') {
                            result = false;
                            break;
                        }
                    } else if (symbol == ')') {
                        if (stack.pop() != '(') {
                            result = false;
                            break;
                        }
                    }
                } else {
                    result = false;
                    break;
                }
            }

            if (stack.size() != 0)
                result = false;

            System.out.println(result);
        }

    }
}
