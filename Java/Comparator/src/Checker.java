import java.util.Comparator;
/*
Comparators are used to compare two objects. In this challenge, you'll create a comparator and use it to sort an array.
The Player class is provided for you in your editor. It has  fields: a  String and a  integer.
Given an array of  Player objects, write a comparator that sorts them in order of decreasing score;
if  or more players have the same score, sort those players alphabetically by name.
To do this, you must create a Checker class that implements the Comparator interface,
then write an int compare(Player a, Player b) method implementing the Comparator.compare(T o1, T o2) method.

Sample Input:
5
amy 100
david 100
heraldo 50
aakansha 75
aleksa 150

Sample Output:
aleksa 150
amy 100
david 100
aakansha 75
heraldo 50
 */
public class Checker implements Comparator<Player> {
    @Override
    public int compare(Player o1, Player o2) {
        if (o1.score == o2.score)
            return o1.name.compareTo(o2.name);
        else if (o1.score > o2.score)
            return -1;
        else
            return 1;
    }
}
