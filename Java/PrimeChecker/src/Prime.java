public class Prime {

    void checkPrime(Integer... args) {
        for (Integer arg : args) {
            if (arg == 2) {
                System.out.print(arg + " ");

            } else if (arg != 1){
                boolean isPrime = true;
                for (int i = 2; i <= Math.sqrt(arg); i++) {
                    if (arg % i == 0) isPrime = false;
                }
                if (isPrime) System.out.print(arg + " ");

            }
        }
        System.out.println();
    }
}
