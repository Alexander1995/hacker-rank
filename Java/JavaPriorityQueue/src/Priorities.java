import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

class Priorities {
    Comparator<Student> comparator = Comparator.comparing(Student::getCGPA).reversed()
            .thenComparing(Student::getName)
            .thenComparing(Student::getID);

    List<Student> getStudents(List<String> events) {
        PriorityQueue<Student> students = new PriorityQueue<>(events.size(), comparator);

        for (String event : events) {
            String[] parsed = event.split("\\s");
            if (!parsed[0].equals("SERVED")) {
                students.add(new Student(Integer.parseInt(parsed[3]), parsed[1], Double.parseDouble(parsed[2])));
            } else if (!students.isEmpty()) {
                students.poll();
            }
        }

        List<Student> list = new ArrayList<>();
        while (!students.isEmpty())
            list.add(students.poll());

        return list;
    }
}
