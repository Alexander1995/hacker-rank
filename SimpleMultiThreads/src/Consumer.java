import java.util.Random;

public class Consumer implements Runnable {
    private Drop drop;

    public Consumer(Drop drop) {
        this.drop = drop;
    }

    public void run() {
        Random random = new Random();
        String message = "";

        while (!message.equals("DONE")) {
            message = drop.take();
            System.out.println("Taken " + message + " message");
            try {
                Thread.sleep(random.nextInt(5000));
            } catch (InterruptedException e) {}
        }

    }
}
